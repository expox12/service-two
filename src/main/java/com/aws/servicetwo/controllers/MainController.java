package com.aws.servicetwo.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/service-two")
public class MainController {

    @GetMapping
    public ResponseEntity<String> whoiam() {
        return ResponseEntity.ok("service-two, OK!");
    }
    
}
